#!/bin/sh
sudo docker build -t mentat-enriched .
sudo docker tag mentat-enriched registry.gitlab.com/protective-h2020-eu/meta-alert-correlation/mentat-enriched-extractor/tree/gmv-dev
sudo docker push registry.gitlab.com/protective-h2020-eu/meta-alert-correlation/mentat-enriched-extractor/tree/gmv-dev
