#!/usr/bin/env python3
# -*- coding: utf-8 -*

from mentat.module.enriched_extractor import EnrichedExtractorDaemon

if __name__ == "__main__":
    EnrichedExtractorDaemon().run()
