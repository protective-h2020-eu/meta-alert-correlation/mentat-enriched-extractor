#!/usr/bin/env python3
# -*- coding: utf-8 -*

import pyzenkit
import zmq

class EnrichedExtractorComponent(pyzenkit.zendaemon.ZenDaemonComponent):

    def get_events(self):
        return [
            {
                'event': 'message_process',
                'callback': self.cbk_event_message_process,
                'prepend': False
            }
        ]

    def cbk_event_message_process(self, daemon, args):
        if 'topic' not in globals():
            global bindOut
            global topic
            topic = "idea_event"
            #"ipc:///tmp/gmv"
            bindOut = "tcp://*:6969"
            self.context = zmq.Context()
            self.socket = self.context.socket(zmq.PUB)
            self.socket.bind(bindOut)

        daemon.logger.info(
            "------>Module enriched_extractor is processing: '{}': '{}'".format(
                args['id'], str(args['data']).strip()
            )
        )
        #send event to zeromq SUB client
        self.socket.send_multipart([bytes("idea_event", "utf8"), bytes(args['data'], "utf8")])
        daemon.logger.info("Alert sent to WSO 2 and ready to storage")
        daemon.queue.schedule('message_commit', args)
        #self.inc_statistic('cnt_printed')
        return (daemon.FLAG_CONTINUE, args)
