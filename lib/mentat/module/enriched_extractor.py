#!/usr/bin/env python3
# -*- coding: utf-8 -*

import mentat.const
import mentat.daemon.piper
import mentat.daemon.component.enriched_extractor
import mentat.daemon.component.parser
import mentat.daemon.component.commiter

from mentat.daemon.component.enriched_extractor import EnrichedExtractorComponent


class EnrichedExtractorDaemon(mentat.daemon.piper.PiperDaemon):

    def __init__(self):
        super().__init__(
            name        = 'mentat-enriched_extractor.py',
            description = 'EnrichedExtractorDaemon - duplicating data from enricher and letting WSO2 consume them',
            path_bin    = '/usr/local/bin',
            path_cfg    = '/etc/mentat',
            path_log    = '/var/mentat/log',
            path_run    = '/var/mentat/run',
            path_tmp    = '/tmp',

            default_config_dir    = None,
            default_queue_in_dir  = '/var/mentat/spool/mentat-enriched_extractor.py',
            default_queue_out_dir = '/var/mentat/spool/mentat-storage.py',

            schedule = [
                (mentat.const.DFLT_EVENT_START,)
            ],
            schedule_after = [
                (mentat.const.DFLT_INTERVAL_STATISTICS, mentat.const.DFLT_EVENT_LOG_STATISTICS)
            ],

            components = [
                mentat.daemon.component.parser.ParserDaemonComponent(),
                EnrichedExtractorComponent(),
                mentat.daemon.component.commiter.CommiterDaemonComponent()
            ]
        )
